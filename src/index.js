const app = require('express')()
const cors = require('cors')
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())

app.listen(3000, '192.168.0.2', error => {
  if (!error) return console.log('Listening on 192.168.0.2:3000')

  return console.log({
    message: 'Error while initiating server.',
    error
  })
})

require('./routes')(app)
